package xyz.crearts.rover.service.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.ByteToMessageDecoder;
import xyz.crearts.dto.EnumCommandGroup;

import java.util.List;

public class CommandDecoder extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        final int readerIndex = byteBuf.readerIndex();
        if (byteBuf.writerIndex() == readerIndex) {
            return;
        }

        ChannelPipeline p = channelHandlerContext.pipeline();
        final byte groupId = byteBuf.getByte(readerIndex);
        final byte id = byteBuf.getByte(readerIndex);

        switch (groupId) {
            case EnumCommandGroup.MOTOR.:
                p.addAfter(channelHandlerContext.name(), null, new CommandEncoder());
                break;
            default:
                byteBuf.skipBytes(byteBuf.readableBytes());
                channelHandlerContext.close();
                return;
        }

        p.remove(this);

    }
}
