package xyz.crearts.rover.service.netty;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import xyz.crearts.dto.MotorCommand;

public class CommandServerHandler extends SimpleChannelInboundHandler<MotorCommand> {
    public static final CommandServerHandler INSTANCE = new CommandServerHandler();

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, MotorCommand motorCommand) throws Exception {

    }
}
