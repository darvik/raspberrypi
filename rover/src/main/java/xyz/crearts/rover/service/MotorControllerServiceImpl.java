package xyz.crearts.rover.service;

import com.pi4j.io.gpio.*;
import com.pi4j.wiringpi.SoftPwm;
import lombok.Builder;
import lombok.Data;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;

@Service
public class MotorControllerServiceImpl implements MotorControllerService {
    private static int MAX_SPEED = 1024;
    @Data
    @Builder
    static public class Motor {
        GpioPinPwmOutput power;
        GpioPinDigitalOutput in1;
        GpioPinDigitalOutput in2;
    }


    final private GpioController gpio;

    final private Motor leftWeel;
    final private Motor rightWeel;

    public MotorControllerServiceImpl() {
        gpio = GpioFactory.getInstance();

        leftWeel = Motor.builder()
                .power(gpio.provisionPwmOutputPin(RaspiPin.GPIO_23, "SPEED", 0))
                .in1(gpio.provisionDigitalOutputPin(RaspiPin.GPIO_24, "FORWARD", PinState.LOW))
                .in2(gpio.provisionDigitalOutputPin(RaspiPin.GPIO_25, "BACKWARD", PinState.LOW))
                .build();


        rightWeel = Motor.builder()
                .power(gpio.provisionPwmOutputPin(RaspiPin.GPIO_26, "SPEED", 0))
                .in1(gpio.provisionDigitalOutputPin(RaspiPin.GPIO_27, "FORWARD", PinState.LOW))
                .in2(gpio.provisionDigitalOutputPin(RaspiPin.GPIO_28, "BACKWARD", PinState.LOW))
                .build();

        leftWeel.power.setShutdownOptions(true);
        leftWeel.in1.setShutdownOptions(true);
        leftWeel.in2.setShutdownOptions(true);

        rightWeel.power.setShutdownOptions(true);
        rightWeel.in1.setShutdownOptions(true);
        rightWeel.in2.setShutdownOptions(true);

        /*
        rightWeel = Motor.builder()
                .power(gpio.provisionDigitalOutputPin(RaspiPin.GPIO_12, PinState.LOW))
                .in1(gpio.provisionDigitalOutputPin(RaspiPin.GPIO_08, PinState.LOW))
                .in2(gpio.provisionDigitalOutputPin(RaspiPin.GPIO_07, PinState.LOW))
                .build();
                */
    }

    public void moveLeft() {
        leftWeel.in1.high();
        leftWeel.in2.low();
        leftWeel.power.setPwm(MAX_SPEED/2);
    }

    public void stopLeft() {
        leftWeel.in1.low();
        leftWeel.in2.low();
        leftWeel.power.setPwm(0);
    }

    public void moveRight() {
        rightWeel.in1.high();
        rightWeel.in2.low();
        rightWeel.power.setPwm(MAX_SPEED/2);
    }

    public void stopRight() {
        rightWeel.in1.low();
        rightWeel.in2.low();
        rightWeel.power.setPwm(0);
    }

    public void move() {
        rightWeel.in1.high();
        rightWeel.in2.low();
        leftWeel.in1.high();
        leftWeel.in2.low();

        rightWeel.power.setPwm(MAX_SPEED/2);
        leftWeel.power.setPwm(MAX_SPEED/2);
    }

    public void stop() {
        stopLeft();
        stopRight();
    }

    @PreDestroy
    public void preDestroy() {
        gpio.shutdown();
    }
}
