package xyz.crearts.rover.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import xyz.crearts.rover.service.MotorControllerServiceImpl;

@Controller
public class IndexController {
    private MotorControllerServiceImpl motor;

    public IndexController(MotorControllerServiceImpl motor) {
        this.motor = motor;
    }

    @GetMapping("/")
    public String indexAction() {
        return "index";
    }

    @GetMapping("/left-move")
    public String moveLeftAction() {
        motor.moveLeft();
        return "index";
    }
    @GetMapping("/left-stop")
    public String stopLeftAction() {
        motor.stopLeft();
        return "index";
    }

    @GetMapping("/right-move")
    public String moveRightAction() {
        motor.moveLeft();
        return "index";
    }
    @GetMapping("/right-stop")
    public String stopRightAction() {
        motor.stopLeft();
        return "index";
    }

    @GetMapping("/move")
    public String moveAction() {
        motor.move();
        return "index";
    }
    @GetMapping("/stop")
    public String stopAction() {
        motor.stop();
        return "index";
    }
}
