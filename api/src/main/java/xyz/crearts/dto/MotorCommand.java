package xyz.crearts.dto;

import lombok.Data;

@Data
public class MotorCommand extends Command{
    public enum Motor {
        MOTOR_LEFT,
        MOTOR_RIGHT
    }

    public enum Direction {
        DIR_FORWARD,
        DIR_BACKWARD,
    }

    private int speed;
    private Direction direction;
    private Motor motor;
}
