package xyz.crearts.dto;

import lombok.Data;

@Data
public class Command {
    private byte groupId;
    private byte id;
}
